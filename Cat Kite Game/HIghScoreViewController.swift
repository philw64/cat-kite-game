//
//  HIghScoreViewController.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/17/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//

import UIKit

class HighScoreViewController: UIViewController {

    @IBOutlet weak var NameAtPos1: UITextField!
    @IBOutlet weak var NameAtPos2: UITextField!
    @IBOutlet weak var NameAtPos3: UITextField!
    @IBOutlet weak var NameAtPos4: UITextField!
    @IBOutlet weak var NameAtPos5: UITextField!
    @IBOutlet weak var NameAtPos6: UITextField!
    @IBOutlet weak var NameAtPos7: UITextField!
    @IBOutlet weak var NameAtPos8: UITextField!
    @IBOutlet weak var NameAtPos9: UITextField!

    @IBOutlet weak var ScorePos1: UILabel!
    @IBOutlet weak var ScorePos2: UILabel!
    @IBOutlet weak var ScorePos3: UILabel!
    @IBOutlet weak var ScorePos4: UILabel!
    @IBOutlet weak var ScorePos5: UILabel!
    @IBOutlet weak var ScorePos6: UILabel!
    @IBOutlet weak var ScorePos7: UILabel!
    @IBOutlet weak var ScorePos8: UILabel!
    @IBOutlet weak var ScorePos9: UILabel!

    @IBOutlet weak var DoneButton: UIButton!
    @IBOutlet weak var TitleLable: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var newScore = AppData().loadNewScore()
        
        let nameOutlets = [
            NameAtPos1,NameAtPos2, NameAtPos3, NameAtPos4, NameAtPos5, NameAtPos6, NameAtPos7, NameAtPos8, NameAtPos9
        ]
        let scoreOutlets = [
            ScorePos1,ScorePos2, ScorePos3, ScorePos4, ScorePos5, ScorePos6, ScorePos7, ScorePos8, ScorePos9
        ]
        let appData = AppData()
        var highScoreTable = appData.loadScoreData()
                    
        if newScore > 0 {
            // Enable Done button
            DoneButton.isHidden = false
            TitleLable.text = "Add Your Name to the High Scores"
            // Find where in the high score table this should go, and enable

            
            var newTable = [AppData.ScoreData]()
            
            if (highScoreTable.count == 0) {
                // Insert new score
                newTable.append(AppData.ScoreData(name: "", score: newScore))
                // Enable Editing for New Name
                nameOutlets[0]?.isEnabled = true
            } else {
                for index in (0...highScoreTable.count-1) {
                    
                    // Index 0 is the top score
                    if (newScore > highScoreTable[index].score) {
                        // Insert new score before this index
                        newTable.append(AppData.ScoreData(name: "", score: newScore))
                        // Enable Editing for New Name
                        nameOutlets[index]?.isEnabled = true
                        nameOutlets[index]?.backgroundColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 1)
                        newScore = 0
                    }
                    // add existing name/topScore at next position in table, if room remains
                    if (newTable.count < highScoreTable.count) {
                        newTable.append(AppData.ScoreData(name: highScoreTable[index].name, score: highScoreTable[index].score))
                    }
                }
            }
            highScoreTable = newTable
            // Clear new Score
            AppData().saveNewScore(newScore: 0)

        }
        // Now fill in the table
        if highScoreTable.count == 0 {
            return
        }
        for index in (0...highScoreTable.count-1) {
            nameOutlets[index]?.text = highScoreTable[index].name
            scoreOutlets[index]?.text = String(highScoreTable[index].score)
        }

    }
    
    @IBAction func DoneClicked(_ sender: Any) {
        saveAndReturn()
    }
    
    @IBAction func ReturnButton(_ sender: Any) {
        saveAndReturn()
    }
    
    func saveTopScores() {
        let nameOutlets = [
            NameAtPos1,NameAtPos2, NameAtPos3, NameAtPos4, NameAtPos5, NameAtPos6, NameAtPos7, NameAtPos8, NameAtPos9
        ]
        let scoreOutlets = [
            ScorePos1,ScorePos2, ScorePos3, ScorePos4, ScorePos5, ScorePos6, ScorePos7, ScorePos8, ScorePos9
        ]
        var highScoreTable = [AppData.ScoreData]()
        for index in (0...AppData.Constants.MaxHighScores.rawValue-1) {
            let name = nameOutlets[index]?.text!
            let score = Int((scoreOutlets[index]?.text)!)

            // Update names and scores from outlets
            highScoreTable.append(AppData.ScoreData(name: name!,score: score!))
        }
        
        let appData = AppData()
        appData.saveScoreData(scores: highScoreTable)
    }

    fileprivate func saveAndReturn() {
        saveTopScores()

        switchToVc(id: "home_view_id")
    }

}

extension UIViewController {
    
    func switchToVc(id: String) {
        var vc : Any?
        if #available(iOS 13.0, *) {
            vc=storyboard?.instantiateViewController(identifier: id)
        } else {
            // Fallback on earlier versions
            vc=storyboard?.instantiateViewController(withIdentifier: id)
        }
        guard let controller = vc as? UIViewController else {return}
        present(controller,animated: true)
    }
}
