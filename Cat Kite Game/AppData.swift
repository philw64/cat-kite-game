//
//  AppData.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/18/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//
import UIKit

class AppData {
    enum Keys : String {
        case HighScoreTable = "CatKiteHighScoreTable"
        case Settings = "CatKiteSettings"
        case NewScore = "CatKiteNewScore"
    }
    
    struct ScoreData:Codable {
        var name: String
        var score: Int
    }
    enum Constants: Int {
        case MaxHighScores=9
    }
    
    func saveScoreData(scores: [ScoreData]) {
        saveCodableData(scores, key: AppData.Keys.HighScoreTable.rawValue)
    }
    
    func loadScoreData() -> [ScoreData] {
        guard let scores : [ScoreData] = getCodableData(key: AppData.Keys.HighScoreTable.rawValue) else {return [ScoreData]()}
        return scores
    }
    
    func saveNewScore(newScore: Int) {
        let defaults = UserDefaults.standard
        defaults.set(newScore,forKey: AppData.Keys.NewScore.rawValue)
    }
    
    func loadNewScore() -> Int {
        let defaults = UserDefaults.standard
        guard let newScore : Int = defaults.value(forKey: AppData.Keys.NewScore.rawValue) as? Int else {return 0}
        return newScore
    }
    
    func saveSettings(dict: [String: Bool]) {
        saveCodableData(dict, key: AppData.Keys.Settings.rawValue)
    }
    
    func loadSettings() -> [String: Bool]? {
        guard let dict : [String: Bool] = getCodableData(key: AppData.Keys.Settings.rawValue) else {return nil}
        return dict
    }
    
    /*
     * Generic Functions to save and load "Codable" app data
     */
    func saveCodableData<T>(_ data: T, key: String) where T : Encodable {
        let preferences = UserDefaults.standard
        let encoder = JSONEncoder()
        do {
            let encodedData = try encoder.encode(data)
            preferences.set(encodedData, forKey: key)
        } catch {
            print("Exception, Failed to save data")
        }
    }
        
    func getCodableData<T>(key: String) -> T? where T : Decodable{
        let preferences = UserDefaults.standard
        guard let encodedData = preferences.data(forKey: key ) else {
            print("Failed to load data")
            return nil
        }
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(T.self, from: encodedData)
            return decodedData
        } catch {
            print("Exception: Failed to get data")
        }
        return nil
                
    }
    
}
