//
//  ViewController.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/14/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let numCards = 6
    var faceDown = 0
    let cardImageNames = ["Red","Blue","Green","Red","Blue","Green"]
    var pointsTimer: Timer?
    var gameTimer: Timer?
    var dieAnimationTimer: Timer?
    var running = false
    var generator: UINotificationFeedbackGenerator?
    var sounds: CatKiteSounds?
    var animationRunning = false
    var settings = [String:Bool]()
    var highScoreTable = [AppData.ScoreData]()
    var graphemeOptions = Set<String>()

    @IBOutlet weak var DieButton: UIButton!
    
    @IBOutlet weak var Card1Button: UIButton!
    @IBOutlet weak var Card2Button: UIButton!
    @IBOutlet weak var Card3Button: UIButton!
    @IBOutlet weak var Card4Button: UIButton!
    @IBOutlet weak var Card5Button: UIButton!
    @IBOutlet weak var Card6Button: UIButton!

    @IBOutlet weak var LevelIndicator: UILabel!
    var cardArray = [UIButton]()
    
    @IBOutlet weak var Player1Score: UILabel!
    @IBOutlet weak var HighScore: UILabel!
    
    @IBOutlet weak var RestartGame: UIButton!
    @IBOutlet weak var SettingsButton: UIButton!
    @IBOutlet weak var InstructionsButton: UIButton!
    @IBOutlet weak var HighScoresButton: UIButton!
    @IBOutlet weak var RulesButton: UIButton!
    
    @IBOutlet weak var PointsCountdown: UILabel!
    @IBOutlet weak var TimeRemaining: UILabel!
    
    @IBOutlet weak var TimeUpSplash: UIImageView!
    
    @IBOutlet weak var PointsStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSettings()

        cardArray = [Card1Button, Card2Button, Card3Button, Card4Button, Card5Button, Card6Button]
        resetGame()

        generator = UINotificationFeedbackGenerator()
        
        sounds = CatKiteSounds()
    }
    
    func loadSettings() {

        let settingsRetrieved = AppData().loadSettings() ?? [
                    "use_c_k"      : true,
                    "use_sc_sk"    : false,
                    "expert_mode"  : false,
                    "vc_rimes"     : true,
                    "vcc_rimes"    : false,
                    "vce_rimes"    : false,
                    "vv_rimes"     : false,
                    "r_controlled" : false,
                    "crcl_words"   : false]

        for (_,keyValue) in settingsRetrieved.enumerated() {
            settings[keyValue.key] = keyValue.value
            switch(keyValue.key) {
            case "use_c_k"   : if keyValue.value {graphemeOptions.insert("use_c_k")}
            case "use_sc_sk" : if keyValue.value {graphemeOptions.insert("use_sc_sk")}
            default: break
            }
        }

        highScoreTable = AppData().loadScoreData()
        if (highScoreTable.count > 0) {
            let topScore = highScoreTable[0]
            HighScore.text = "\(topScore.score)"
        } else {
            HighScore.text = "0"
        }
            
    }
   
    /**
     * User Interface buttons, steppers, etc.
     */
    @IBAction func Card1Tapped(_ sender: Any) {
        processCardTapped(card: Card1Button)
    }
    
    @IBAction func Card2Tapped(_ sender: Any) {
        processCardTapped(card: Card2Button)
    }
    
    @IBAction func Card3Tapped(_ sender: Any) {
        processCardTapped(card: Card3Button)
    }
    
    @IBAction func Card4Tapped(_ sender: Any) {
        processCardTapped(card: Card4Button)
    }
    
    @IBAction func Card5Tapped(_ sender: Any) {
        processCardTapped(card: Card5Button)
    }
    
    @IBAction func Card6Tapped(_ sender: Any) {
        processCardTapped(card: Card6Button)
    }
        
    func setStartButtonText(text: String, color: String?) {
        RestartGame.setTitle(text,for: .normal)
        if (color == "Black") {
            RestartGame.setTitleColor(UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 1),for: .normal)
        } else if (color == "Red") {
            RestartGame.setTitleColor(UIColor(displayP3Red: 1, green: 0, blue: 0, alpha: 1),for: .normal)
        }
        
    }
    
    fileprivate func startGame() {
        running = true
        SettingsButton.isHidden = true
        InstructionsButton.isHidden = true
        HighScoresButton.isHidden = true
        RulesButton.isHidden = true
        PointsStack.isHidden = false
        DieButton.isHidden = false
        gameTimer = Timer.scheduledTimer(timeInterval: 1.0, target:self, selector: #selector(gameTimerCountdown), userInfo: nil, repeats: true)
        setUpCards()
        changeDie()
    }
    
    @IBAction func RestartButton(_ sender: Any) {
        let buttonText = RestartGame.title(for: .normal) ?? String("")
        if (buttonText == "Start") {
            setStartButtonText(text: "Restart",color: "Black")
            TimeRemaining.text = "60"

            if (gameTimer != nil) {
                 gameTimer!.invalidate()
            }
            startGame()
        } else if (buttonText == "Really?") {
            resetGame()
            setStartButtonText(text: "Start",color: "Black")
        } else {
            setStartButtonText(text: "Really?",color: "Red")
        }
    }
    
    @IBAction func DieClicked(_ sender: Any) {
        // Ignore taps until game started
        if (!running || animationRunning) {
            errorFeedback()
            return
        }
        
        changeDie()
        resetPointsTimer()
    }
    
    

    
    @IBAction func HighScoresTapped(_ sender: Any) {
        switchToVc(id: "high_scores_id")
    }

    @IBAction func SettingsTapped(_ sender: Any) {
        switchToVc(id: "settings_view_id")
    }
    
    @IBAction func InstructionsTapped(_ sender: Any) {
        switchToVc(id: "instructions_id")
    }
    
    @IBAction func RulesButtonTapped(_ sender: Any) {
        switchToVc(id: "rules_id")
    }
    
        
    /*
      * Refactored Card Action Handling
      */
     func processCardTapped(card: UIButton) {
         // Ignore taps until game started
         if (!running || animationRunning) {
             errorFeedback()
             return
         }
        
         setStartButtonText(text: "Restart",color: "Black")
        
         // Ignore taps if card is face down
         guard let background: UIImage = card.backgroundImage(for : .normal) else {return}
         
         if (background == UIImage(named: "back") ||
             background == UIImage(named: "Check") ) {
             return
         }

         let oldTitle = card.title(for: .normal) ?? String("")
         let newTitle = DieButton.title(for: .normal) ?? String("")
        
        
        // print("processTap \(card)")
        if WordLists().validateWord(word: newTitle + oldTitle, options: graphemeOptions) {
             card.setTitle(newTitle + oldTitle, for: .normal)
             self.successAnimation(card: card)

             let points = Int(PointsCountdown.text ?? "0")
             resetPointsTimer()
             addToScore(num: points!)
             changeDie()
         } else {
             self.errorAnimation(card: card)
         }
     }

    /*
     * Refactored functions called from multiple places
     */
    func resetGame() {
        updateHighScore()
        stopGame()

        Player1Score.text = "0"
        resetCards()
        changeDie()
    }
    
    func updateHighScore() {
        let newScore = Int(Player1Score.text ?? "0")!
        if newScore == 0 {
            return
        }
        
        AppData().saveNewScore(newScore: newScore)        
        switchToVc(id: "high_scores_id")
        
        if highScoreTable.count > 0 {
            let topScore = highScoreTable[0]
            HighScore.text = "\(topScore.score)"
        }
    }

    func resetCards() {
        for card in cardArray {
            card.setBackgroundImage(UIImage(named: "back"), for: .normal)
        }
    }
  
    func setUpCards() {
        let all_rimes = WordLists().getRimes()
        

        var usedIndexes = [Int]()
        
        for card in cardArray {
            var index: Int
            var removeRControlled = false
            var rimes = [String]()
            for (_,keyValue) in settings.enumerated() {
                if graphemeOptions.contains("use_c_k") {
                    switch(keyValue.key) {
                    case "vc_rimes"    :
                        if keyValue.value {
                            rimes += all_rimes["c_vc_rimes"]! + all_rimes["k_vc_rimes"]!
                        }
                    case "vcc_rimes"   :
                        if keyValue.value {
                            rimes += all_rimes["c_vcc_rimes"]! + all_rimes["k_vcc_rimes"]!
                        }
                    case "vce_rimes"   :
                        if keyValue.value {
                            rimes += all_rimes["c_vce_rimes"]! + all_rimes["k_vce_rimes"]!
                        }
                    case "vv_rimes"    :
                        if keyValue.value {
                            rimes += all_rimes["c_vv_rimes"]! + all_rimes["k_vv_rimes"]!
                        }
                    case "crcl_words"  :
                        if keyValue.value {rimes += all_rimes["cr_blends"]! + all_rimes["cl_blends"]!}
                    default: break
                    }
                }
                if graphemeOptions.contains("use_sc_sk") {
                    switch(keyValue.key) {
                    case "vc_rimes"    :
                        if keyValue.value {
                            rimes += all_rimes["sc_vc_rimes"]! + all_rimes["sk_vc_rimes"]!
                        }
                    case "vcc_rimes"   :
                        if keyValue.value {
                            rimes += all_rimes["sc_vcc_rimes"]! + all_rimes["sk_vcc_rimes"]!
                        }
                    case "vce_rimes"   :
                        if keyValue.value {
                            rimes += all_rimes["sc_vce_rimes"]! + all_rimes["sk_vce_rimes"]!
                        }
                    case "vv_rimes"    :
                        if keyValue.value {
                            rimes += all_rimes["sc_vv_rimes"]! + all_rimes["sk_vv_rimes"]!
                        }
                    default: break
                    }
                }
                if (keyValue.key == "r_controlled") {
                    removeRControlled = !keyValue.value
                }
            }
            // Remove duplicates
            rimes = Array(Set(rimes))
            // Remove R-Controlled vowels if disabled
            if (removeRControlled) {
                var newRimes = [String]()
                for rime in rimes {
                    let range = NSRange(location: 0, length: rime.count)
                    let regex = try! NSRegularExpression(pattern: "[aeiou]r")
                    if (regex.firstMatch(in: rime, options:[], range: range) == nil ) {
                        newRimes.append(rime)
                    }
                }
                rimes = newRimes
            }

            repeat {
                index = Int.random(in: 0...rimes.count-1)
            } while usedIndexes.contains(index)
            usedIndexes.append(index)
            card.setTitle(rimes[index], for: .normal)
            let nameIndex = cardArray.firstIndex(of: card) ?? 0
            card.setBackgroundImage(UIImage(named: cardImageNames[nameIndex]), for: .normal)
        }
        faceDown = 0
    }
    
    func addToScore(num: Int) {
       let oldScore = Int(Player1Score.text ?? "0")!
       
       Player1Score.text = String(oldScore + num)
    }

    // Used by changeDie to figure out weighting
    private struct Counts {
        var c_count: Int
        var k_count: Int
        var sc_count: Int
        var sk_count: Int
        
        init() {
            c_count = 0
            k_count = 0
            sc_count = 0
            sk_count = 0
        }
        func total() -> Int {
            return c_count + k_count + sc_count + sk_count
        }
        func dump() {
            print("counts: c=\(c_count)")
            print("counts: kc=\(k_count)")
            print("counts: sc=\(sc_count)")
            print("counts: sk=\(sk_count)")
            print("Total: \(total())")
        }
    }

    func changeDie() {
  
        // Calculate C-K-SC-SK weighting
        let removeRControlled = !settings["r_controlled"]!
        
        // Sum up how many of the rimes need c, k, sc, or sk graphemes
        var counts = Counts()
        for (_,keyValue) in settings.enumerated() {

            switch(keyValue.key) {
            case "vc_rimes"     :
            if (keyValue.value) {
                counts.c_count += 14
                counts.k_count += 5
                counts.sc_count += 7
                counts.sk_count += 5
            }
            case "vcc_rimes"    :
            if (keyValue.value) {
                counts.c_count += removeRControlled ? 19: 27
                counts.k_count += 8
                counts.sc_count += removeRControlled ? 6 : 10
                counts.sk_count += removeRControlled ? 5 : 6
            }
           case "vce_rimes"    :
            if (keyValue.value) {
                counts.c_count += removeRControlled ? 4 : 6
                counts.k_count += 4
                counts.sc_count += removeRControlled ? 4 : 5
                counts.sk_count += 2
            }
           case "vv_rimes"     :
            if (keyValue.value) {
                counts.c_count += 4
                counts.k_count += 1
                counts.sk_count += 2
            }
            case "crcl_words"   :
            if (keyValue.value) {
                counts.c_count += removeRControlled ? 51 : 109
            }
           default: break
           }
        }

        if (!settings["use_c_k"]!) {
            counts.c_count = 0
            counts.k_count = 0
        }
        if (!settings["use_sc_sk"]!) {
            counts.sc_count = 0
            counts.sk_count = 0
        }
        // counts.dump()

/*         let graphemeIdx = Int.random(in: 0...counts.total()-1)
        var grapheme = "c"
        
        if graphemeIdx >= (counts.c_count + counts.k_count + counts.sc_count) {
            grapheme = "sk"
        } else if graphemeIdx >= (counts.c_count + counts.k_count) {
            grapheme = "sc"
        } else if graphemeIdx >= counts.c_count {
            grapheme = "k"
        }
  */
        let c_weight = settings["use_c_k"]! ? 3 : 0
        let k_weight = settings["use_c_k"]! ? 1 : 0
        let sc_weight = settings["use_sc_sk"]! ? 1 : 0
        let sk_weight = settings["use_sc_sk"]! ? 1 : 0
        
        let graphemeIdx = Int.random(in: 0...c_weight + k_weight + sc_weight + sk_weight-1)
        var grapheme = "c"

        if graphemeIdx >= (c_weight + k_weight + sc_weight) {
             grapheme = "sk"
         } else if graphemeIdx >= (c_weight + k_weight) {
             grapheme = "sc"
         } else if graphemeIdx >= c_weight {
             grapheme = "k"
         }
        
        runDieAnimation(grapheme: grapheme)
        if (pointsTimer != nil) {
           pointsTimer!.invalidate()
        }
        
        let expert =  (nil != settings["expert_mode"]) ? settings["expert_mode"]! : false
        let timeInterval = expert ? 1.0 : 2.0
        resetPointsTimer()
        pointsTimer = Timer.scheduledTimer(timeInterval: timeInterval, target:self, selector: #selector(pointsCountdown), userInfo: nil, repeats: true)
    }

    func resetPointsTimer() {
        PointsCountdown.text = "5"
    }
    
    func successFeedback()
    {
        if (generator != nil) {
            generator!.notificationOccurred(.success)
        }
        if (sounds != nil) {
            sounds?.playSound(sound: CatKiteSounds.Sounds.Success)
        }
    }
    
    func errorFeedback()
    {
        if (generator != nil) {
            generator!.notificationOccurred(.error)
        }
        if (sounds != nil) {
            sounds?.playSound(sound: CatKiteSounds.Sounds.Error)
        }
    }
    /*
     * Timers and Timer Hooks
     */
    @objc func pointsCountdown() {
        if (!running) {
            return
        }
        let points = Int(PointsCountdown.text ?? "0")
        if (points! > 1) {
            PointsCountdown.text = String(points! - 1)
        } else {
            resetPointsTimer()
            changeDie()
        }
    }
    
    fileprivate func stopGame() {
        running = false
        SettingsButton.isHidden = false
        InstructionsButton.isHidden = false
        HighScoresButton.isHidden = false
        RulesButton.isHidden = false
        PointsStack.isHidden = true
        DieButton.isHidden = true
    }
    
    @objc func gameTimerCountdown() {
        if (!running) {
            return
        }
        let seconds = Int(TimeRemaining.text ?? "0")
        if (seconds! > 0) {
            TimeRemaining.text = String(seconds! - 1)
        } else {
            stopGame()

            setStartButtonText(text: "Start",color: "Black")
            if (sounds != nil) {
                sounds?.playSound(sound: CatKiteSounds.Sounds.GameWin)
            }
            displayForTime(image: TimeUpSplash, time: 4.0)
            updateHighScore()
            resetCards()
        }
    }
    
    func successAnimation(card: UIButton) {
        card.setBackgroundImage(UIImage(named: "Check"), for: .normal)
        let context = ["card" : card]
        Timer.scheduledTimer(timeInterval: 0.8, target:self, selector: #selector(flipCard), userInfo: context, repeats: false)
        successFeedback()
    }
    
    @objc func flipCard(timer: Timer) {
        guard let context = timer.userInfo as? [String: UIButton] else {return}
        
        guard let card : UIButton = context["card"] else {return}
        card.setBackgroundImage(UIImage(named: "back"), for: .normal)
        card.setTitle("", for: .normal)
        faceDown += 1
        if (faceDown == numCards) {
            addToScore(num: 5)
            setUpCards()
        }
        
    }
    
    func errorAnimation(card: UIButton) {
        animationRunning = true // Prevent other key presses
        card.setBackgroundImage(UIImage(named: "flash"), for: .normal)
        let context = ["card" : card]
        Timer.scheduledTimer(timeInterval: 2.0, target:self, selector: #selector(flashError), userInfo: context, repeats: false)
        errorFeedback()
    }
    
    @objc func flashError(timer: Timer) {
        guard let context = timer.userInfo as? [String: UIButton] else {return}
        
        guard let card : UIButton = context["card"] else {return}
        let nameIndex = cardArray.firstIndex(of: card) ?? 0
        card.setBackgroundImage(UIImage(named: cardImageNames[nameIndex]), for: .normal)
        animationRunning = false
    }
    
    
    fileprivate func setDieImage(image: String) {
        DieButton.setBackgroundImage(UIImage(named: image), for: .normal)
    }
    
    func runDieAnimation(grapheme: String){
       let context = ["grapheme" : grapheme,"index": 3] as [String : Any]
        animationRunning = true
        dieAnimationTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector: #selector(updateDieGrapheme), userInfo: context, repeats: false)
        DieButton.setTitle("",for: .normal)
        setDieImage(image: "Die_shuffle_4")

    }
    
    @objc func updateDieGrapheme(timer: Timer) {
        let context = timer.userInfo as? [String: Any]
        
        guard let index     : Int      = context!["index"]     as? Int else {return}
        guard let grapheme  : String   = context!["grapheme"]  as? String else {return}
        if index > 0 {
            // restart timer
            let context = ["grapheme" : grapheme, "index": index-1] as [String : Any]
            setDieImage(image: "Die_shuffle_\(index)")

            dieAnimationTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector: #selector(updateDieGrapheme), userInfo: context, repeats: false)
        } else {
            // Set grapheme and don't restart timer.
            DieButton.setTitle(grapheme,for: .normal)
            setDieImage(image: "dieButton")
            animationRunning = false
        }
        
    }

    
    func displayForTime(image: UIImageView, time: Double) {
        image.isHidden = false
        let context = ["image" : image]

        Timer.scheduledTimer(timeInterval: time, target:self, selector: #selector(hideAgain), userInfo: context, repeats: false)
    }
    
    @objc func hideAgain(timer: Timer) {
        guard let context = timer.userInfo as? [String: UIImageView] else {return}
        
        guard let image : UIImageView = context["image"] else {return}
        
        image.isHidden = true
    }
}

