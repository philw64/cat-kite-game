//
//  RulesViewController.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/17/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {

    @IBOutlet weak var InstructionsText: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        InstructionsText.scrollRangeToVisible(NSMakeRange(
                                              0,1))
    }

    @IBAction func ReturnButton(_ sender: Any) {
        switchToVc(id: "home_view_id")
    }
    
}
