//
//  AppSounds.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/16/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//

import UIKit
import AVFoundation

class CatKiteSounds {
    enum Sounds: UInt32 {
        case Success=1306
        case Error=1057
        case GameWin=1025
    }
    
    func playSound(sound: CatKiteSounds.Sounds) {
        AudioServicesPlaySystemSound(sound.rawValue)
    }
}

