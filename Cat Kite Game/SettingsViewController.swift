//
//  SettingsViewController.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/16/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    @IBOutlet weak var Option_C_K: UISwitch!
    @IBOutlet weak var Option_SC_SK: UISwitch!
    @IBOutlet weak var Option_Expert_Mode: UISwitch!
    
    @IBOutlet weak var Option_VC: UISwitch!
    @IBOutlet weak var Option_VCC: UISwitch!
    @IBOutlet weak var Option_VCE: UISwitch!
    @IBOutlet weak var Option_VV: UISwitch!
    @IBOutlet weak var Option_RControlled: UISwitch!
    @IBOutlet weak var Option_CRCL: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settingsRetrieved = AppData().loadSettings() ?? [
                    "use_c_k"      : true,
                    "use_sc_sk"    : false,
                    "expert_mode"  : false,
                    "vc_rimes"     : true,
                    "vcc_rimes"    : false,
                    "vce_rimes"    : false,
                    "vv_rimes"     : false,
                    "r_controlled" : false,
                    "crcl_words"   : false]
        
        for (_,keyValue) in settingsRetrieved.enumerated() {
            switch(keyValue.key) {
            case "use_c_k":      Option_C_K.isOn = keyValue.value
            case "use_sc_sk":    Option_SC_SK.isOn = keyValue.value
            case "expert_mode":  Option_Expert_Mode.isOn = keyValue.value
            case "vc_rimes":     Option_VC.isOn = keyValue.value
            case "vcc_rimes":    Option_VCC.isOn = keyValue.value
            case "vce_rimes":    Option_VCE.isOn = keyValue.value
            case "vv_rimes":     Option_VV.isOn = keyValue.value
            case "r_controlled": Option_RControlled.isOn = keyValue.value
            case "crcl_words":   Option_CRCL.isOn = keyValue.value
            default: print("SettingsViewController: Unexpected option loaded: \(keyValue)")
            }
        }
    }
    
    func saveSettings() {
        let settings : [String:Bool] = [
        "use_c_k": Option_C_K.isOn,
        "use_sc_sk" : Option_SC_SK.isOn,
        "expert_mode" : Option_Expert_Mode.isOn,
        "vc_rimes": Option_VC.isOn,
        "vcc_rimes":Option_VCC.isOn,
        "vce_rimes":Option_VCE.isOn,
        "r_controlled":Option_RControlled.isOn,
        "vv_rimes":Option_VV.isOn,
        "crcl_words":Option_CRCL.isOn
        ]
                    
        AppData().saveSettings(dict: settings)
    }

    @IBAction func ReturnButton(_ sender: Any) {
        saveSettings()
        switchToVc(id: "home_view_id")
    }
    
    
}
