//
//  WordLists.swift
//  Cat Kite Game
//
//  Created by Philip Wooldridge on 9/22/20.
//  Copyright © 2020 Pollinate. All rights reserved.
//

import Foundation

class WordLists {
    func getRimes() -> [String: [String]] {
        let rimes = [
            "c_vc_rimes"   : ["ub", "at", "op", "og", "an", "ut", "up", "on", "am", "ap", "ob", "ot", "od", "ab"],
            "k_vc_rimes"   : ["it", "in", "eg", "ip", "id"],
            "sc_vc_rimes"  : ["ud", "um", "ab", "ar", "am", "an", "at"],
            "sk_vc_rimes"  : ["ip", "im", "in", "id", "it"],

            "c_vcc_rimes"  : ["amp", "ost", "ash", "ant", "uff", "onk", "osh", "urb", "url", "ask", "ast", "olt", "old", "ord", "orn", "ork", "ult", "arp", "art", "ard", "omb", "usp", "atch", "onch"],
            "k_vcc_rimes"  : ["ick", "ing", "ilt", "iss", "ept", "ink", "ill"],
            "sc_vcc_rimes" : ["old", "orn", "amp", "off", "uff", "ary", "arf", "alp", "orch"],
            "sk_vcc_rimes" : ["int", "iff", "imp", "irt", "ill", "etch"],
            
            "c_vv_rimes"   : ["ow", "aw", "ue", "oo", ],
            "k_vv_rimes"   : ["ey"],
            "sc_vv_rimes"  : [],
            "sk_vv_rimes"  : ["ew", "y"],

            "c_vvc_rimes"  : ["owl", "oal", "oil", "oop", "oot", "ool"],
            "k_vvc_rimes"  : ["een", "eel", "iln", "elp", ],
            "sc_vvc_rimes" : ["oop", "oot", "out", "owl", "our"],
            "sk_vvc_rimes" : ["ied", "ier"],

            "c_vce_rimes"  : ["are", "one", "ave", "ane", "ure", "oke", "ase", "ube", "ake", "ode", "ape", "ute", "ame", "age", "ove"],
            "k_vce_rimes"  : ["ite"],
            "sc_vce_rimes" : ["ore", "ope", "are", "ale", "one"],
            "sk_vce_rimes" : ["ive"],

            "cr_blends": ["rew", "rowd", "rown", "reak", "ream", "rook", "rack", "rew", "ress", "rest", "rick", "ram", "roon", "risp", "rept", "ramp", "rick", "razy", "raze", "ramp", "rone", "roft", "rime", "rimp", "rude", "ried", "rier", "ries", "rawl", "reek", "reed", "reep", "rank", "row", "rop", "ry", "rock", "rag", "rab", "ram", "rate", "rib", "rack", "rust", "rush", "rown", "rash", "rass", "roak", "roak", "rumb", "ruel", "rave", "rypt", "raft", "rone", "ross", "rank", "rane"],
            "cl_blends": ["lown", "lock", "lack", "luck", "lose", "limb", "lick", "lip", "lan", "lam", "lad", "lay", "law", "lap", "ling", "link", "lamp", "lick", "lout", "loud", "lone", "left", "lod", "log", "lot", "liff", "lank", "lang", "lop","oth", "lash", "lass", "lasp", "loak", "lump", "lub", "lue", "lear", "leat", "lean", "lerk", "laim", "lung", "lunk", "love", "lonk", "lone", "lank", "lash"],
            
        ]
        return rimes
    }
    
    func getWordList(options: Set<String>) -> [String] {
        let all_rimes = getRimes()
        var words = [String()]
        for (key,rimes) in all_rimes {
            if options.contains("use_c_k") {
                switch(key) {
                case "c_vc_rimes"  : for rime in rimes {words.append("c"+rime)}
                case "k_vc_rimes"  : for rime in rimes {words.append("k"+rime)}

                case "c_vcc_rimes" : for rime in rimes {words.append("c"+rime)}
                case "k_vcc_rimes" : for rime in rimes {words.append("k"+rime)}

                case "c_vv_rimes"  : for rime in rimes {words.append("c"+rime)}
                case "k_vv_rimes"  : for rime in rimes {words.append("k"+rime)}

                case "c_vvc_rimes" : for rime in rimes {words.append("c"+rime)}
                case "k_vvc_rimes" : for rime in rimes {words.append("k"+rime)}

                case "c_vce_rimes" : for rime in rimes {words.append("c"+rime)}
                case "k_vce_rimes" : for rime in rimes {words.append("k"+rime)}

                case "cr_blends"   : for rime in rimes {words.append("c"+rime)}
                case "cl_blends"   : for rime in rimes {words.append("c"+rime)}
                default: break
                }
            }
            if options.contains("use_sc_sk") {
                switch(key) {
                case "sc_vc_rimes"  : for rime in rimes {words.append("sc"+rime)}
                case "sk_vc_rimes"  : for rime in rimes {words.append("sk"+rime)}

                case "sc_vcc_rimes" : for rime in rimes {words.append("sc"+rime)}
                case "sk_vcc_rimes" : for rime in rimes {words.append("sk"+rime)}

                case "sc_vv_rimes"  : for rime in rimes {words.append("sc"+rime)}
                case "sk_vv_rimes"  : for rime in rimes {words.append("sk"+rime)}

                case "sc_vvc_rimes" : for rime in rimes {words.append("sc"+rime)}
                case "sk_vvc_rimes" : for rime in rimes {words.append("sk"+rime)}

                case "sc_vce_rimes" : for rime in rimes {words.append("sc"+rime)}
                case "sk_vce_rimes" : for rime in rimes {words.append("sk"+rime)}

                default: break
                }
            }
        }
        return words

    }
    
    func validateWord(word: String, options: Set<String>) -> Bool {

        let words = getWordList(options: options)
        return words.contains(word)
    }
}
